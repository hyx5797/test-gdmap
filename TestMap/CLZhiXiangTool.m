//
//  CLZhiXiangTool.m
//  zhiXiangRong
//
//  Created by 致享科技 on 2018/10/10.
//  Copyright © 2018年 致享科技. All rights reserved.
//

#import "CLZhiXiangTool.h"

@implementation CLZhiXiangTool
//十六进制转RGB颜色
+ (UIColor *)colorWithHexString:(NSString *)stringToConvert withAlpha:(CGFloat)alpha{
    NSString *cString = [[stringToConvert stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    if ([cString hasPrefix:@"#"]) {
        cString = [cString substringFromIndex:1];
    }
    if ([cString length] != 6 && [cString length] != 8) {
        return [UIColor clearColor];
    }
    unsigned int r = 255.0f, g = 255.0f, b = 255.0f, a = 255.0f;
    [[NSScanner scannerWithString:[cString substringWithRange:NSMakeRange(0, 2)]] scanHexInt:&r];
    [[NSScanner scannerWithString:[cString substringWithRange:NSMakeRange(2, 2)]] scanHexInt:&g];
    [[NSScanner scannerWithString:[cString substringWithRange:NSMakeRange(4, 2)]] scanHexInt:&b];
    if ([cString length] == 8) {
        [[NSScanner scannerWithString:[cString substringWithRange:NSMakeRange(6, 2)]] scanHexInt:&a];
    }
    return [UIColor colorWithRed:((float) r / 255.0f)
                           green:((float) g / 255.0f)
                            blue:((float) b / 255.0f)
                           alpha:((float) alpha)];
}

// 传入字符串，计算其宽度及高度
+ (CGSize)getStringSizeWithStr:(NSString *)str withFont:(NSInteger)font withFontName:(NSString *)fontName withMaxWidth:(CGFloat)maxWidth withMaxHeight:(CGFloat)maxHeight{
    CGSize size = CGSizeMake(0, 0);
    if (str != nil && ![str isEqualToString:@""]) {
        size = [str boundingRectWithSize:CGSizeMake(maxWidth, maxHeight) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:[UIFont fontWithName:fontName size:font]} context:nil].size;
        size.height = size.height + 1;
        size.width = size.width + 1;
    }
    return size;
}

@end
