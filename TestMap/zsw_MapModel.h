//
//  zsw_MapModel.h
//  CL_test
//
//  Created by 笨笨黄 on 2022/10/28.
//  Copyright © 2022 CL. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface zsw_MapModel : NSObject
@property (nonatomic,strong)NSString *name;//财智中心6栋A座
@property (nonatomic,assign)double latitude;//纬度
@property (nonatomic,assign)double longitude;
@property (nonatomic,strong)NSString *address;//@"云华路与吉泰四路交叉路口东南侧"
@property (nonatomic, assign) NSInteger distance;
@property (nonatomic,strong)NSString *province;//四川省
@property (nonatomic,strong)NSString *pcode;
@property (nonatomic,strong)NSString *city;//成都市
@property (nonatomic,strong)NSString *citycode;
@property (nonatomic,strong)NSString *district;//武侯区
@property (nonatomic,strong)NSString *adcode;
@end

NS_ASSUME_NONNULL_END
