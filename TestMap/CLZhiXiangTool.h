//
//  CLZhiXiangTool.h
//  zhiXiangRong
//
//  Created by 致享科技 on 2018/10/10.
//  Copyright © 2018年 致享科技. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface CLZhiXiangTool : NSObject

//十六进制转RGB颜色
+ (UIColor *)colorWithHexString:(NSString *)stringToConvert withAlpha:(CGFloat)alpha;


// 传入字符串，计算其宽度及高度
+ (CGSize)getStringSizeWithStr:(NSString *)str withFont:(NSInteger)font withFontName:(NSString *)fontName withMaxWidth:(CGFloat)maxWidth withMaxHeight:(CGFloat)maxHeight;


@end
