//
//  MapDemoVC.h
//  CL_test
//
//  Created by 笨笨黄 on 2022/10/28.
//  Copyright © 2022 CL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "zsw_MapModel.h" //保存位置
NS_ASSUME_NONNULL_BEGIN

@interface MapDemoVC : UIViewController
@property (nonatomic, copy) void(^savaLocationAddress)(zsw_MapModel *model_map);
@property (nonatomic, copy) void(^savaLocationAddressTwo)(zsw_MapModel *model_map,UIImage *url);
@property (nonatomic,strong)NSString *latitude_map;
@property (nonatomic,strong)NSString *longitude_map;
@end

NS_ASSUME_NONNULL_END
