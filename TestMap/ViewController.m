//
//  ViewController.m
//  TestMap
//
//  Created by 笨笨黄 on 2022/10/31.
//

#import "ViewController.h"
//#import "Masonry.h"
#import <AMapLocationKit/AMapLocationKit.h>
#import <AMapFoundationKit/AMapFoundationKit.h>
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];

    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:@"点击跳转" forState:UIControlStateNormal];
    btn.backgroundColor = [UIColor redColor];
    btn.frame = CGRectMake(120, 200, 300, 300);
    [btn addTarget:self action:@selector(btnOpenMapClick) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn];
   
}
-(void)btnOpenMapClick{

    if ([CLLocationManager locationServicesEnabled] && ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedAlways)) {
        
        MapDemoVC *dateVC = [[MapDemoVC alloc] init];
        dateVC.savaLocationAddress = ^(zsw_MapModel * _Nonnull model_map) {
            
        };
        [self.navigationController pushViewController:dateVC animated:YES];
    }else{
        //1.提醒用户检查当前的网络状况        //2.提醒用户打开定位开关
        UIAlertController* alertVC = [UIAlertController alertControllerWithTitle:@"允许\"定位\"提示"message:@"方便更精确的定位到你附近的砂场"preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"打开定位" style:UIAlertActionStyleDefault handler:^(UIAlertAction*_Nonnullaction) {                //打开定位设置
            NSURL*settingsURL = [NSURL URLWithString:UIApplicationOpenSettingsURLString];         [[UIApplication sharedApplication]openURL:settingsURL];            }];
        UIAlertAction * cancel = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            
        }];
        [alertVC addAction:cancel];
        [alertVC addAction:ok];
        [self presentViewController:alertVC animated:YES completion:nil];
    }
}
@end
